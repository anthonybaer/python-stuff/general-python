flowers = [
    "Daffodil",
    "evening primrose",
    "hydrangea",
    "iris",
    "lavender",
    "sunflower",
    "tiger lily",
    ]

# for flower in flowers:
#     print(flower)

separator = ", "
output = separator.join(flowers)
print(output)

print(",".join(flowers))