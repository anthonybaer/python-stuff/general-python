# data = [4, 5, 104, 105, 110, 120, 130, 130, 150,
#         160, 170, 183, 187, 188, 191, 350, 360]
#

# data = [4, 5, 104, 105, 110, 120, 130, 130, 150,
#         160, 170, 183, 187, 188, 191]

# data = [104, 105, 110, 120, 130, 130, 150,
#         160, 170, 183, 187, 188, 191, 350, 360]

# data = [4, 5, 104, 105, 110, 120, 130, 130, 150,
#         160, 170, 183, 187, 188, 191, 350, 360]

data = [
        1160, 1111, 2222, 22222]

min_valid = 100
max_valid = 200

#process the low values in the list
stop = 0
for index, value in enumerate(data):
    # for index, value in enumerate(data):
    if value >= min_valid:
        stop = index
        break

print(stop)
del data[:stop]
print(data)

#process high values in list
start = 0
for index in range(len(data) - 1, -1, -1):
    if data[index] <= max_valid:
        # we have the index on the last item to keep.
        # set start to the position of the first item to delete
        #w which is one less than the number to keep
        start = index + 1
        break

print(start) # for debugging
del data[start:]
print(data)

