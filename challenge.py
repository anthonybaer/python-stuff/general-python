

print("Please choose your option from the list below: \n")
print("1. Learn Python")
print("2. Learn Java")
print("3. Go swimming")
print("4. Have dinner")
print("5. Go to bed")
print("0. Exit")

choice = input("Please select a number: "))
while choice != 0:
    if choice < 6:
        print("That's a valid option")
        break
    else:
        print("Please try again")
