name = input("What is your name? ")

age = int(input("What is your age? "))

# if age >= 18 and age <= 30:

if age in range(16, 66):
    print("Welcome to Tommy's Holiday Camp, " + name + " !")
else:
    print("Sorry, " + name + ", you can't go. :(")
